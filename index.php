<?php
interface Shape {
    public function area();
}

class Circle implements Shape {
    private $radius;

    public function __construct($radius) {
         $this -> radius = $radius;
    }

    public function area() {
        return $this -> radius * $this -> radius * pi();
    }
}

class Rectangle implements Shape {
    private $width;
    private $height;

    public function __construct($width,$height) {
        $this -> width  = $width;
        $this -> height = $height;
    }

    public function area() {
        return $this -> width * $this -> height;
    }
}

class Triangle implements Shape {
    private $base;
    private $height;

    public function __construct($base,$height) {
        $this -> base  = $base;
        $this -> height = $height;
    }

    public function area() {
        return 1/2 * $this-> base * $this -> height;
    }
}

echo "Implements class Shape and encapsulation";
echo "<br>";
echo "Rectangle= ";
$rect = new Rectangle(3,3);
echo $rect->area();
echo "<br>";
echo "Circle= ";
$circ = new Circle (5);
echo $circ->area();
echo "<br>";
echo "Triangle= ";
$trian = new Triangle (4,3);
echo $trian->area();

class ShapeService
{
    public function getArea(Shape $shape_type)
    {
        return $shape_type->area();
    }
}

$shape = new ShapeService();
echo "<br>";echo "<br>";
echo "Shape service and polymorphism";
echo "<br>";
echo "Rectangle= ";
echo $shape->getArea(new Rectangle(3,3));
echo "<br>";
echo "Circle= ";
echo $shape->getArea(new Circle (5));
echo "<br>";
echo "Triangle= ";
echo $shape->getArea(new Triangle (4,3));
